
//cleanArray removes all duplicated elements
function cleanArray(array) {
  var i, j, len = array.length, out = [], obj = {};
  for (i = 0; i < len; i++) {
    obj[array[i]] = 0;
  }
  for (j in obj) {
    out.push(j);
  }
  return out;
}

angular.module('shelldoc', ['ngRoute', 'ngSanitize'])
.config(function($routeProvider) {
  $routeProvider
    .when('/', {
      controller:'AppliListController',
      templateUrl:'list_appli.html',
    })
  	.when('/:appli/:version/:fichier', {
        controller:'DocFichierController',
        templateUrl:'display_doc.html',
  	})
  	.when('/:appli/:version/:fichier/:fonction', {
        controller:'DocFichierController',
        templateUrl:'display_doc.html',
  	})
  	.when('/:appli/:version', {
        controller:'AppliModuleListController',
        templateUrl:'list_module.html',
  	})
    .otherwise({
      redirectTo:'/'
	});
})
.config(['$httpProvider', function($httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    $httpProvider.defaults.headers.get['Accept-Charset'] = undefined;
}])
.filter('trust', [
    '$sce',
    function($sce) {
      return function(value, type) {
        // Defaults to treating trusted text as `html`
        return $sce.trustAs(type || 'html', value);
      }
    }
])
.controller('AppliListController', ['$scope', '$http', '$window', function($scope, $http, $window) {
	var versionsCSV=$http.get('Documents_data/versions.csv',{headers : {'Content-Type' : 'text/csv; charset=iso-8859-1'}}).then(function(response) {
	  applications = {};
	  versions = {};
	  
      var data = $("<div>").html(response.data).text();
      allTextLines = data.split(/\r\n|\n/);
      for(i=1; i < allTextLines.length; i++) {
        line = allTextLines[i].split(";");

        version = {};
        version.appli = line[0];
        line.shift();
        version.version = line[0];
        line.shift();
        version.description = line[0];
        if(version.appli != "") {
				 if(applications[version.appli] === undefined) {
					 applications[version.appli] = [];
				 }
				 applications[version.appli].push(version);
				 versions[version.appli] = version;
        }
      }
	  $scope.applications = applications;
	  $scope.versions = versions;
  	    console.log("Applications:");
	    console.log($scope.applications);
		console.log("Versions:");
		console.log($scope.versions);
    });
}])
.controller('DocFichierController', ['$scope','$sce', '$anchorScroll','$location','$http', '$routeParams', '$sanitize', '$filter', '$window', '$timeout', function($scope, $sce, $anchorScroll,$location, $http, $routeParams, $sanitize, $filter, $window, $timeout) {
  $scope.appli = $routeParams.appli;
  $scope.version = $routeParams.version;
  $scope.fichier = $routeParams.fichier;
  $scope.fonction = $routeParams.fonction;

  $scope.to_trusted = function(html_code) {
     return $sce.trustAsHtml(html_code);
   }

  $scope.navigateTo = function(tag) {
    $anchorScroll('fonction' + tag);    
  }

    var indexationCSV=$http.get('Documents_data/' + $scope.appli + '/' + $scope.version + '/indexation.csv',{headers : {'Content-Type' : 'text/csv; charset=iso-8859-1'}}).then(function(response) {
      console.log(response.data);
      $scope.pages = [];

      var data = $("<div>").html(response.data).text();
      allTextLines = data.split(/\r\n|\n/);
      for(i=1; i < allTextLines.length; i++) {
        line = allTextLines[i].split(";");

        page = {};
        page.nomFichier = line[0];
        line.shift();
        page.nomFonction = line[0];
        line.shift();
        page.descriptionFonction = line[0];
        if(page.nomFichier === $scope.fichier) {
  			     $scope.pages.push(page);
        }
      }
      console.log($scope.pages)
    });
  // calcul nom fichier markdown depuis le nom du fichier sh
  $scope.fichierMarkdown = $scope.fichier.substring(0,$scope.fichier.length - 2) + 'md';
  var indexationCSV=$http.get('Documents_data/' + $scope.appli + '/' + $scope.version + '/' + $scope.fichierMarkdown,{headers : {'Content-Type' : 'text/markdown; charset=iso-8859-1'}}).then(function(response) {
    //console.log(response.data);
    var converter = new showdown.Converter();
    converter.setOption('literalMidWordUnderscores', true);
    converter.setOption('tables', true);
    $scope.documentation = converter.makeHtml(response.data);
    $scope.documentation = $scope.documentation.replace(/<table>/g, "<table class=\"table\">");
    $scope.documentation = $scope.documentation.replace(/<blockquote>/g, "<div class=\"highlight\"><blockquote  class=\"blockquote\">");
    $scope.documentation = $scope.documentation.replace(/<\/blockquote>/g, "</blockquote></div>");
    $scope.documentation = $scope.documentation.replace(/<code>/g, "<div class=\"highlight\"><code  class=\"code\">");
    $scope.documentation = $scope.documentation.replace(/<\/code>/g, "</code></div>");


    console.log( $scope.documentation);
	
	// Ajout d'un timeout avant scrool sur la fonction
	if($scope.fonction !== undefined)
	{
	  $timeout(function() { $anchorScroll('fonction' + $scope.fonction); }, 500);
	}
    
  });
}])
.controller('AppliModuleListController', ['$scope','$http', '$routeParams', '$sanitize', '$filter', '$window', function($scope, $http, $routeParams, $sanitize, $filter, $window) {
  console.log($routeParams.appli);
  $scope.appli = $routeParams.appli;
  $scope.version = $routeParams.version;

  var indexationCSV=$http.get('Documents_data/' + $scope.appli + '/' + $scope.version + '/indexation.csv',{headers : {'Content-Type' : 'text/csv; charset=iso-8859-1'}}).then(function(response) {
    console.log(response.data);
    $scope.pages = [];

    var data = $("<div>").html(response.data).text();
    allTextLines = data.split(/\r\n|\n/);
    for(i=1; i < allTextLines.length; i++) {
      line = allTextLines[i].split(";");

      page = {};
      page.nomFichier = line[0];
      line.shift();
      page.nomFonction = line[0];
      line.shift();
      page.descriptionFonction = line[0];
      if(page.nomFichier != "") {
			     $scope.pages.push(page);
      }
    }
    console.log($scope.pages)
  });
}]);
